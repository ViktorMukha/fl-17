
const arr = [1, 3, 2,4, 5];
function getMaxEventElement(arr) {
    arr.sort((x, y) => y - x);
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] % 2 === 0){
            return arr[i];
        }
    }
}
console.log(getMaxEventElement(arr));

let a = 3;
let b = 5;
b = [a, a = b][0];
console.log(a, b);

function getValue(value){
    return value !== undefined ? value : '-';
}
console.log(getValue(null))

let obj = [
    ['name', 'dan'],
    ['age', '21'],
    ['city', 'lviv']
];
function objectToArray (obj) {
    return Object.keys(obj).map(prop => [prop, obj[prop]]);
}
console.log(objectToArray(obj));


const obj1 = [{
    name:'nick'
}]
function giveId() {
    let unknowns = obj1.map((user, index) => ({
        ...user,
        id: index + 1
    }));
    console.log(unknowns);
}
giveId(obj1)
console.log(obj1);


const oldObj = {
    name:'willow',
    details:{
        id:1,
        age:47,
        university:'LNU'
    }
}

const regroupedObject = ({
    name: firstName,
    detail:{
        id,
        age,
        university
    }
}) => {
    let restructed = {
        university,
        user:{ age,firstName,id }
    }
    return restructed
}


let mas = ["1", "1", "2", "3", "3", "1", 'a','b','a'];
let unique = mas.filter((item, i, ar) => ar.indexOf(item) === i);
console.log(unique);


const phoneNumber = '0123456789'
function cutNumber (cut){
 const firstNumbers = cut.slice(-4);
 const hieghNumber = firstNumbers.padStart(phoneNumber.length,'*')
     return hieghNumber
}
cutNumber(phoneNumber)


const add = (a,b) => {
    if (!a || !b){
        throw new Error('is required')
    }
    return a+b;
}
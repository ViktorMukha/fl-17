'use strict';
class PizzaExeption{
    constructor(log) {
        this.log = log;
    }

}
/**
 * Class
 * @constructor
 * @param size - size of pizza
 * @param type - type of pizza
 * @throws {PizzaException} - in case of improper use
 */


class Pizza{
    constructor(size,type) {
        const isTwoArgument = arguments.length === 2
        if (!isTwoArgument){
            throw new PizzaExeption(`required only two arguments, given:${arguments.length}`)
        }

        const isAllowedType = Pizza.allowedTypes.includes(type.name);
        if (!isAllowedType){
            throw new PizzaExeption('Invalid type')
        }

        const isAllowedSize = Pizza.allowedSizes.includes(size.name);
        if (!isAllowedSize){
            throw new PizzaExeption('Invalid size')
        }

        this.size = size;
        this.type = type;
        this.extraIngredients = [];
    }
	addExtraIngredient(ingredient){
            const isOneArgument = arguments.length === 1
        if (!isOneArgument){
            throw new PizzaExeption('required only one argument')
        }


         const isAllowed = Pizza.allowedExtraIngredients.includes(ingredient.name);
         if (!isAllowed){
             throw new PizzaExeption('Invalid ingredient')
         }
        if (!this.extraIngredients.includes(ingredient)){
            this.extraIngredients.push(ingredient);
        } else {
            throw new PizzaExeption('Duplicate ingredient')
        }
    }

	removeExtraIngredient(ingredient){
        const isOneArgument = arguments.length === 1
        if (!isOneArgument){
            throw new PizzaExeption('recuaried only one argument')
        }

        const isAllowed = Pizza.allowedExtraIngredients.includes(ingredient.name);
        if (!isAllowed){
            throw new PizzaExeption('Invalid ingredient')
        }

        if (this.extraIngredients.includes(ingredient)){
            this.extraIngredients = this.extraIngredients.filter(element => element !== ingredient);
        } else {
            throw new PizzaExeption('No ingredient')
        }
    }

	getSize(){
        return this.size;
    }

	getPrice(){
         const extraIngredientsPrice = this.extraIngredients.reduce((price,element) => price + element.price, 0)
         const totalPrice = this.size.price + this.type.price + extraIngredientsPrice;
         return totalPrice
    }

	getPizzaInfo(){
        const ingridients = this.extraIngredients.map(el => el.name).join(',')
        const totalInfo =`Size:${this.size.name}; type:${this.type.name}; extra ingredients:${ingridients}; 
        price:${this.getPrice()}UAN`
        return totalInfo;
    }

}


// function Pizza(size, type) { ... }

/* Sizes, types and extra ingredients */
Pizza.SIZE_S = {
    price: 50,
    name: 'SMALL'
};
Pizza.SIZE_M ={
    price: 75,
    name: 'MEDIUM'
};
Pizza.SIZE_L ={
    price: 100,
    name: 'LARGE'
};

Pizza.TYPE_VEGGIE = {
    price: 50,
    name: 'VEGGIE'
};
Pizza.TYPE_MARGHERITA = {
    price: 60,
    name: 'MARGHERITA'
};
Pizza.TYPE_PEPPERONI = {
    price: 70,
    name: 'PEPPERONI'
};

Pizza.EXTRA_TOMATOES = {
    price: 5,
    name: 'TOMATOES'
};
Pizza.EXTRA_CHEESE = {
    price: 7,
    name: 'CHEESE'
};
Pizza.EXTRA_MEAT = {
    price: 9,
    name: 'MEAT'
};

/* Allowed properties */
Pizza.allowedSizes = ['SMALL','MEDIUM','LARGE'];
Pizza.allowedTypes = ['VEGGIE','MARGHERITA','PEPPERONI'];
Pizza.allowedExtraIngredients = ['TOMATOES','CHEESE','MEAT'];

let pizza = new Pizza(Pizza.SIZE_S,Pizza.TYPE_VEGGIE);
pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
pizza.addExtraIngredient(Pizza.EXTRA_TOMATOES);
pizza.getPrice()
pizza.getPizzaInfo()



/**
 * Provides information about an error while working with a pizza.
 * details are stored in the log property.
 * @constructor
 */
//function PizzaException(...) { ... }


/* It should work */ 
// // small pizza, type: veggie
// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// // add extra meat
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// // check price
// console.log(`Price: ${pizza.getPrice()} UAH`); //=> Price: 109 UAH
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_CHEESE);
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_TOMATOES);
// // check price
// console.log(`Price with extra ingredients: ${pizza.getPrice()} UAH`); // Price: 121 UAH
// // check pizza size
// console.log(`Is pizza large: ${pizza.getSize() === Pizza.SIZE_L}`); //=> Is pizza large: false
// // remove extra ingredient
// pizza.removeExtraIngredient(Pizza.EXTRA_CHEESE);
// console.log(`Extra ingredients: ${pizza.getExtraIngredients().length}`); //=> Extra ingredients: 2
// console.log(pizza.getPizzaInfo()); //=> Size: SMALL, type: VEGGIE; extra ingredients: MEAT,TOMATOES; price: 114UAH.

// examples of errors
// let pizza = new Pizza(Pizza.SIZE_S); // => Required two arguments, given: 1

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.SIZE_S); // => Invalid type

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Duplicate ingredient

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Invalid ingredient
